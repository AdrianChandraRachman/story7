asgiref==3.2.10
astroid==2.4.2
certifi==2020.6.20
chardet==3.0.4
Click==7.0
colorama==0.4.3
coverage==5.3
cycler==0.10.0
dj-database-url==0.5.0
Django==3.1.1
django-crispy-forms==1.9.2
django-environ==0.4.5
Flask==1.1.1
gunicorn==20.0.4
idna==2.10
isort==5.5.2
itsdangerous==1.1.0
Jinja2==2.10.3
kiwisolver==1.1.0
lazy-object-proxy==1.5.1
lxml==4.4.2
MarkupSafe==1.1.1
matplotlib==3.1.1
mccabe==0.6.1
MouseInfo==0.1.2
numpy==1.17.2
Pillow==6.2.1
psycopg2==2.8.6
psycopg2-binary==2.8.6
PyAutoGUI==0.9.48
pygame==1.9.6
PyGetWindow==0.0.7
pylint==2.6.0
PyMsgBox==1.0.7
pynput==1.0.3
pyparsing==2.4.2
pyperclip==1.7.0
PyRect==0.1.4
PyScreeze==0.1.25
python-dateutil==2.8.0
PyTweening==1.0.3
pytz==2020.1
requests==2.24.0
selenium==3.141.0
six==1.15.0
sqlparse==0.3.1
toml==0.10.1
typed-ast==1.4.1
urllib3==1.25.10
webbot==0.1.4
Werkzeug==0.16.0
whitenoise==5.2.0
wrapt==1.12.1
WTForms==2.2.1
