$(".label > span").click(
    function() {
        let contentGetter = $(this).parent().next()
        contentGetter.toggleClass("active")
    }
)

$(".up").click(
    function() {
        let upTransport = $(this).parent().parent()
        upTransport.prev().insertAfter(upTransport)
    }
)

$(".down").click(
    function() {
        let downTransport = $(this).parent().parent()
        downTransport.next().insertBefore(downTransport)
    }
)
